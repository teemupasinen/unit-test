const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined

    before(() => {
        myvar = 1;
        console.log('before testing');
    })

    it('myvar should exist', () => {
        should.exist(myvar);
    })

    it('should return 2 when using sum fuction a=1, b=1', () => {
        const result = mylib.sum(1,1); // 1 + 1
        expect(result).to.equal(2); // result expected to be 2
    });

    it('Assert foo in not bar', () => {
        assert('foo' !== 'bar')
    })

    after(() => {
        console.log('after testing');
    })
})